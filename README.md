# MoviesCrud

Este curso express te permite introducirte de manera rapidísima a 3 nuevas tecnologías: Angular 6, Laravel 5.6 y Bootstrap 4. Crearemos un CRUD para gestionar una base de datos de películas y con esto aprenderás la base fundamental de una web app moderna.

A través de un ejemplo práctico, se te muestra cómo:

Configurar tu ambiente para correr un backend usando Apache, PHP y MySQL tanto en Mac como en Windows

Crear una app de Laravel y usar artisan para generar tus recursos y acceso a datos

Usar las características que Laravel ofrece para servir un API REST

Configurar tu ambiente para usar Angular 6

Crear una app de Angular desde cero

Crear servicios para acceder a los datos de Laravel

Lo que aprenderás
Crear un app full stack básica
Aprender acerca de lo básico de Laravel 5.6
Aprender lo más importante de Angular 6
Utilizar de manera efectiva los estilos de Bootstrap 4
¿Hay requisitos para crear un curso?
HTML
CSS
JavaScript
PHP
¿Para quién es este curso?
Un desarrollador de backend que desee convertirse en full stack
Un desarrollador de frontend que desee convertirse en full stack
Un desarrollador que desee aprender Angular